# How to start this project
1. Create `python` environment and activate it.
1. Install dependencies `pip install -r requirements.txt`
1. start `Django` server `python manage.py runserver`
1. Open new terminal for `ReactJS`
1. In the new terminal, redirect to `frontend` directory
1. Install dependencies `npm install`
1. Start React app with `npm start`
# Running with docker
1. `git checkout containerization`
1. `docker-compose up`
1. access the website via `http://192.168.18.2:81/`